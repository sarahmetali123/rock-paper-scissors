const daButtons = document.querySelectorAll('#daButtons button');
const playerres = document.getElementById('player');
const computerres = document.getElementById('computer');
const gameres = document.getElementById('result');

let player;
let computer;

const choices = ['rock', 'paper', 'scissors'];

daButtons.forEach(button => {
    button.addEventListener('click', event => {
        player = button.textContent.toLowerCase();
        console.log('Player chose', player);
        computer = computerChoice();
        console.log('Computer chose', computer);
        determineWinner(player, computer);
    });
});

function computerChoice() {
    const randomIndex = Math.floor(Math.random() * 3);
    return choices[randomIndex];
}

function determineWinner(player, computer) {
    playerres.textContent = `Player: ${player}`;
    computerres.textContent = `Computer: ${computer}`;

    if (player === computer) {
        gameres.textContent = 'Result: It\'s a tie!';
        console.log('It\'s a tie!');
    } else if (
        (player === 'rock' && computer === 'scissors') ||
        (player === 'scissors' && computer === 'paper') ||
        (player === 'paper' && computer === 'rock')
    ) {
        gameres.textContent = 'Result: Player wins!';
        console.log('Player wins!');
    } else {
        gameres.textContent = 'Result: Computer wins!';
        console.log('Computer wins!');
    }
}